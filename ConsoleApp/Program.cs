﻿namespace ConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using FFXIVInventory.Core;
    using FFXIVInventory.Core.Service;
    using Sharlayan.Core;
    using Sharlayan.Models.ReadResults;

    internal class Program
    {
        private readonly InventoryScanner scanner;

        private readonly PlayerCharacterService pcService;

        private readonly ActorService actorService;

        public Program(string dataDir, string serverHost, short serverPort)
        {
            this.scanner = new InventoryScanner(dataDir);
            HttpClient httpClient = new HttpClient();
            this.pcService = new PlayerCharacterService(serverHost, serverPort, httpClient);
            this.actorService = new ActorService(serverHost, serverPort, httpClient);
        }

        public static void Main(string[] args)
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Log("FFXIV Inventory Tool");

            if (args.Length < 3)
            {
                throw new ArgumentException("One or more arguments are missing");
            }

            Program program = new Program(args[0], args[1], short.Parse(args[2], NumberFormatInfo.InvariantInfo));
            do
            {
                program.RunAsync().Wait();
            }
            while (true);
        }

        public async Task RunAsync()
        {
            Log("Starting...");
            await this.AttachAsync().ConfigureAwait(false);
            do
            {
                await this.SendCurrentPlayerCharacterStateAsync().ConfigureAwait(false);
                await this.SendActorsStateAsync().ConfigureAwait(false);
                await this.SendCurrentInventoryStateAsync().ConfigureAwait(false);
                Thread.Sleep(5000);
            }
            while (InventoryScanner.IsAttached());
            Log("Quiting...");
        }

        private static void Log(string message)
        {
            Trace.TraceInformation($"Thread-{Thread.CurrentThread.ManagedThreadId} - {message}");
        }

        private async Task<bool> AttachAsync()
        {
            do
            {
                bool scanned = await this.scanner.Scan().ConfigureAwait(false);
                if (scanned)
                {
                    return scanned;
                }

                Thread.Sleep(5000);
            }
            while (true);
        }

        private async Task SendCurrentPlayerCharacterStateAsync()
        {
            CurrentPlayer player = InventoryScanner.ReadPlayerCharacter();
            if (player == null)
            {
                return;
            }

            try
            {
                await this.pcService.UpdateAsync(player).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Log($"Failed to update player data: {e.GetType()} - {e.Message}");
                return;
            }

            Log("Updated player data");
        }

        private async Task SendActorsStateAsync()
        {
            CurrentPlayer player = InventoryScanner.ReadPlayerCharacter();
            if (player == null)
            {
                return;
            }

            try
            {
                ActorResult actors = InventoryScanner.ReadActors();
                var playerActor = new List<ActorItem>(actors.CurrentPCs.Values).Find(item => item.Name.Equals(player.Name, System.StringComparison.Ordinal));
                await this.actorService.UpdateActorsAsync(actors.CurrentNPCs.Values, playerActor).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Log($"Failed to send actors state: {e.GetType()} - {e.Message}");
                return;
            }

            Log("Actors state has been sent");
        }

        private async Task SendCurrentInventoryStateAsync()
        {
            CurrentPlayer player = InventoryScanner.ReadPlayerCharacter();
            if (player == null)
            {
                return;
            }

            InventoryResult inventoryResult = InventoryScanner.ReadInventory();
            if (inventoryResult == null)
            {
                return;
            }

            Log($"Found {inventoryResult.InventoryContainers.Count} inventories");

            try
            {
                await this.pcService.UpdateAsync(inventoryResult, player.Name).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Log($"Failed to update inventory data: {e.Message}");
                return;
            }

            Log($"Updated inventory data");
        }
    }
}
