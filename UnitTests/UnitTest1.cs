﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharlayan;
using Sharlayan.Core;
using Sharlayan.Core.Enums;
using Sharlayan.Models;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void FuncTest()
        {
            Func<string> foo = () => "abc";
            foo.BeginInvoke(delegate { }, foo);
        }

        [TestMethod]
        public void TryMemoryParser()
        {
            Trace.TraceInformation($"x64 process: {Environment.Is64BitProcess}");

            Directory.SetCurrentDirectory("F:/games/FFXIV/ffxiv-tools/ffxiv-monitor/metadata");

            Process[] processes = Process.GetProcessesByName("ffxiv_dx11");
            Trace.TraceInformation("found " + processes.Length + " processes");
            if (processes.Length > 0)
            {
                // supported: English, Chinese, Japanese, French, German, Korean
                string gameLanguage = "Chinese";
                // whether to always hit API on start to get the latest sigs based on patchVersion, or use the local json cache (if the file doesn't exist, API will be hit)
                bool useLocalCache = true;
                // patchVersion of game, or latest
                string patchVersion = "4.2.1";
                Process process = processes[0];
                ProcessModel processModel = new ProcessModel
                {
                    Process = process,
                    IsWin64 = true
                };

                MemoryHandler.Instance.SignaturesFoundEvent += (source, e) =>
                {
                    Trace.TraceInformation($"signature scan result: {e.Signatures}, cost: {e.ProcessingTime}");
                };
                MemoryHandler.Instance.ExceptionEvent += (source, e) =>
                {
                    Trace.TraceError($"exception ocurred: {e.Exception}, {e.Exception.StackTrace}");
                };
                MemoryHandler.Instance.SetProcess(processModel, gameLanguage, patchVersion, useLocalCache, false);

                System.Threading.Thread.Sleep(2 * 1000);

                Trace.TraceInformation($"can read inventories: {Reader.CanGetInventory()}");

                //scanData();
                showPlayerMem();
                //printInventories();
                //showInventoryAddress();
                //compareSnapshots();
            }

            void printInventories()
            {
                var inventories = Reader.GetInventory();
                Trace.TraceInformation("read " + inventories.InventoryContainers.Count + " inventories");
                //printInterestItems(inventories.InventoryContainers);

                foreach (var inventory in inventories.InventoryContainers)
                {
                    Trace.TraceInformation($"inventory amount={inventory.Amount}, type={inventory.Type}, typeid={inventory.TypeID}, itemCount={inventory.Items.Count}");
                }

                foreach (var item in inventories.InventoryContainers[0].Items)
                {
                    Trace.TraceInformation($"item amount={item.Amount}, id={item.ID}, slot={item.Slot}, hq={item.IsHQ}, dur={item.DurabilityPercent}");
                }
            }

            void compareSnapshots()
            {
                var inventories = Reader.GetInventory();

                System.Threading.Thread.Sleep(1 * 1000);

                var inventories2 = Reader.GetInventory();

                for (int i = 0; i < 32; i++)
                {
                    compare(inventories.InventoryContainers[i], inventories2.InventoryContainers[i]);
                }
            }

            void printInterestItems(List<InventoryContainer> inventories)
            {
                var inventory = inventories[1];
                var item4 = inventory.Items[3];
                var item5 = inventory.Items[4];
                printItem(item4, $"inventory {inventory.Type} item 3");
                printItem(item5, $"inventory {inventory.Type} item 4");
            }

            void printItem(InventoryItem item, string key)
            {
                Trace.TraceInformation($"{key}: slot={item.Slot}, id={item.ID}, amount={item.Amount}, sb={item.SB}, dur={item.Durability}, hq={item.IsHQ}");
            }

            void compare(InventoryContainer snapshot1, InventoryContainer snapshot2)
            {
                var type = snapshot1.Type;
                compareValue(snapshot1.Amount, snapshot2.Amount, $"inventory {type} amount");
                compareValue(snapshot1.Items.Count, snapshot2.Items.Count, $"inventory {type} itemCount");
                int count = snapshot1.Items.Count > snapshot2.Items.Count ? snapshot2.Items.Count : snapshot1.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    compareItem(snapshot1.Items[i], snapshot2.Items[i], type, i);
                }
            }

            void compareValue(Object v1, Object v2, string name)
            {
                if (!v1.Equals(v2))
                {
                    Trace.TraceInformation($"{name}: {v1} => {v2}");
                }
            }

            void compareItem(InventoryItem item1, InventoryItem item2, Inventory.Container type, int index)
            {
                compareValue(item1.Amount, item2.Amount, $"inventory {type} item {index} amount");
                compareValue(item1.Durability, item2.Durability, $"inventory {type} item {index} durability");
                compareValue(item1.GlamourID, item2.GlamourID, $"inventory {type} item {index} glamourId");
                compareValue(item1.ID, item2.ID, $"inventory {type} item {index} id");
                compareValue(item1.SB, item2.SB, $"inventory {type} item {index} SB");
                compareValue(item1.Slot, item2.Slot, $"inventory {type} item {index} slot");
                compareValue(item1.IsHQ, item2.IsHQ, $"inventory {type} item {index} HQ");
            }
        }

        public void scanData()
        {
            var signatures = new List<Signature>();
            var sig = new Signature
            {
                Key = "myitem",
                Value = "B355"
                //Value = "D066EC8E"
                //Value = "604CFF2C"
            };
            signatures.Add(sig);
            Scanner.Instance.LoadOffsets(signatures, true);

            System.Threading.Thread.Sleep(20 * 1000);

            byte[] value = new byte[96 * 3];
            IntPtr address = IntPtr.Subtract(Scanner.Instance.Locations["myitem"], sig.Offset);
            //IntPtr address = new IntPtr(3496406158);
            address = IntPtr.Subtract(address, 7);
            MemoryHandler.Instance.Peek(address, value);
            Trace.TraceInformation($"data scanned at {address.ToInt64().ToString("X")}: {BitConverter.ToString(value)}");
        }

        void showPlayerMem()
        {
            var PlayerInfoMap = (IntPtr) Scanner.Instance.Locations[Signatures.PlayerInformationKey];
            var source = MemoryHandler.Instance.GetByteArray(PlayerInfoMap, 614);
            var value = BitConverter.ToString(source, 0);
            Trace.TraceInformation($"player mem: {value}");
        }

        void showInventoryAddress()
        {
            var firstInventoryPointer = new IntPtr(MemoryHandler.Instance.GetPlatformUInt(Scanner.Instance.Locations[Signatures.InventoryKey]));
            var inventoryPointer = firstInventoryPointer;
            for (int index = 0; index < 90; index++)
            {
                var bytes = new byte[24];
                MemoryHandler.Instance.Peek(inventoryPointer, bytes);
                var inventoryData = BitConverter.ToString(bytes, 0);
                int capacity = MemoryHandler.Instance.GetByte(inventoryPointer, 12);
                var firstItemAddress = MemoryHandler.Instance.GetPlatformUInt(inventoryPointer);

                Trace.TraceInformation($"inventory block {index} at {inventoryPointer.ToInt64().ToString("X")}: {inventoryData}, capacity={capacity}, item-addr={firstItemAddress.ToString("X")}");

                //unrecoginized: 7,8,10-18
                if (index == -1)
                {
                    showInventoryItemsMemory(firstItemAddress, capacity);
                }

                inventoryPointer = IntPtr.Add(inventoryPointer, 24);
            }

        }

        void showInventoryItemsMemory(long address, int capacity)
        {
            for (int index = 0; index < capacity; index++)
            {
                var itemOffset = new IntPtr(address + index * 56);

                var bytes = new byte[56];
                MemoryHandler.Instance.Peek(itemOffset, bytes);
                var value = BitConverter.ToString(bytes, 0);
                Trace.TraceInformation($"item block {index}: {value}");

            }
        }
    }
}
