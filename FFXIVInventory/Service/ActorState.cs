﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class ActorState
    {
        public string OwnerKey { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
