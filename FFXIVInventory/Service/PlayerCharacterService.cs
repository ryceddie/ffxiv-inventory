﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Sharlayan.Core;
    using Sharlayan.Core.Enums;
    using Sharlayan.Models.ReadResults;

    public class PlayerCharacterService
    {
        private readonly string url;

        private readonly HttpClient client;

        public PlayerCharacterService(String host, short port, HttpClient client)
        {
            this.url = $"http://{host}:{port}/api/player-characters";
            this.client = client;
        }

        public async Task UpdateAsync(InventoryResult inventoryResult, string cname)
        {
            Inventories inventories = ToApiModel(inventoryResult);
            string json = JsonConvert.SerializeObject(inventories);

            // Trace.TraceInformation($"inventories: {json}");
            StringContent content = new StringContent(json, Encoding.UTF8);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;charset=utf-8");
            HttpResponseMessage response = await this.client.PutAsync($"{this.url}/{cname}/inventories", content).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception($"server responded with error: {response.StatusCode} {response.ReasonPhrase}");
            }
        }

        public async Task UpdateAsync(CurrentPlayer player)
        {
            PlayerCharacter pc = ToApiModel(player);
            string json = JsonConvert.SerializeObject(pc);
            Trace.TraceInformation($"player: {json}");
            StringContent content = new StringContent(json, Encoding.UTF8);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;charset=utf-8");
            HttpResponseMessage response = await this.client.PutAsync($"{this.url}/{player.Name}", content).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception($"server responded with error: {response.StatusCode} {response.ReasonPhrase}");
            }
        }

        private static void Log(string message)
        {
            Trace.TraceInformation($"Thread-{Thread.CurrentThread.ManagedThreadId} - {message}");
        }

        private static Inventories ToApiModel(InventoryResult inventoryResult)
        {
            Inventories result = new Inventories();
            foreach (InventoryContainer ic in inventoryResult.InventoryContainers)
            {
                if (ic.Items.Count == 0)
                {
                    continue;
                }

                List<Cell> cells = new List<Cell>();
                foreach (InventoryItem item in ic.Items)
                {
                    cells.Add(new Cell(Convert.ToUInt32(item.Slot), item.ID.ToString(), item.IsHQ, item.Amount));
                }

                result.Listing.Add(new Service.Inventory(ic.Type.ToString(), cells));
            }

            return result;
        }

        private static PlayerCharacter ToApiModel(CurrentPlayer pc)
        {
            List<string> jobs = new List<string> { "ACN", "ALC" };
            PlayerCharacter result = new PlayerCharacter();
            result.Name = pc.Name;
            result.JobAbbr = pc.Job.ToString();
            var jobNames = Enum.GetNames(typeof(Actor.Job));
            new List<string>(jobNames).GetRange(1, jobNames.Length - 1).ForEach(job =>
            {
                var property = pc.GetType().GetProperty(job);
                if (property == null)
                {
                    // Log($"CurrentPlayer missing property: {job}");
                    return;
                }
                byte level = (byte)property.GetValue(pc);
                result.JobLevels.Add(job, level);
            });
            /*
            result.JobLevels.Add("ACN", pc.ACN);
            result.JobLevels.Add("ALC", pc.ALC);
            result.JobLevels.Add("ARC", pc.ARC);
            result.JobLevels.Add("ARM", pc.ARM);
            result.JobLevels.Add("AST", pc.AST);
            result.JobLevels.Add("BSM", pc.BSM);
            result.JobLevels.Add("BTN", pc.BTN);
            result.JobLevels.Add("CNJ", pc.CNJ);
            result.JobLevels.Add("CPT", pc.CPT);
            result.JobLevels.Add("", pc.CUL);
            result.JobLevels.Add("", pc.DRK);
            result.JobLevels.Add("", pc.FSH);
            result.JobLevels.Add("", pc.GLD);
            result.JobLevels.Add("", pc.GSM);
            result.JobLevels.Add("", pc.LNC);
            result.JobLevels.Add("", pc.LTW);
            result.JobLevels.Add("", pc.MCH);
            result.JobLevels.Add("", pc.MIN);
            result.JobLevels.Add("MRD", pc.MRD);
            result.JobLevels.Add("PGL", pc.PGL);
            result.JobLevels.Add("", pc.RDM);
            result.JobLevels.Add("", pc.ROG);
            result.JobLevels.Add("", pc.SAM);
            result.JobLevels.Add("", pc.THM);
            result.JobLevels.Add("", pc.WVR);
            */
            return result;
        }
    }
}
