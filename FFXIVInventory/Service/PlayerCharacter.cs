﻿namespace FFXIVInventory.Core.Service
{
    using System.Collections.Generic;

    public class PlayerCharacter
    {
        public PlayerCharacter()
        {
            this.JobLevels = new Dictionary<string, ushort>();
        }

        public string Name { get; set; }

        public string JobAbbr { get; set; }

        public Dictionary<string, ushort> JobLevels { get; }
    }
}
