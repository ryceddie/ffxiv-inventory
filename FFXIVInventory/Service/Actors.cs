﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Actors
    {
        public Actors()
        {
            this.Listing = new List<ActorState>();
        }

        public List<ActorState> Listing { get; }
    }
}
