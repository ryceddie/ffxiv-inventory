﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Cell
    {
        public Cell(uint index, string id, bool hq, uint quantity)
        {
            this.Index = index;
            this.Id = id;
            this.Hq = hq;
            this.Quantity = quantity;
        }

        public uint Index { get; }

        public string Id { get; }

        public bool Hq { get; }

        public uint Quantity { get; }
    }
}
