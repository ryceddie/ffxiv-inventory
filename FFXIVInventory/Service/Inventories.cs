﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Inventories
    {
        public Inventories()
        {
            this.Listing = new List<Inventory>();
        }

        public List<Inventory> Listing { get; set; }
    }
}
