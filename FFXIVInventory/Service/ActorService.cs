﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Sharlayan.Core;

    public class ActorService
    {
        private readonly string url;

        private readonly HttpClient client;

        public ActorService(string host, short port, HttpClient client)
        {
            this.url = $"http://{host}:{port}/api/actors";
            this.client = client;
        }

        public async Task UpdateActorsAsync(ICollection<ActorItem> actorItems, ActorItem pc)
        {
            Actors actors = ToApiModel(actorItems, pc);
            string json = JsonConvert.SerializeObject(actors);
            Trace.TraceInformation($"actor states: {json}");
            StringContent content = new StringContent(json, Encoding.UTF8);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;charset=utf-8");
            HttpResponseMessage response = await this.client.PutAsync(this.url, content).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception($"server responded with error: {response.StatusCode} {response.ReasonPhrase}");
            }
        }

        private static Actors ToApiModel(ICollection<ActorItem> actorItems, ActorItem pc)
        {
            Actors result = new Actors();
            foreach (var item in actorItems)
            {
                result.Listing.Add(new ActorState()
                {
                    OwnerKey = pc.Name,
                    Name = item.Name,
                    Type = item.Type.ToString().ToUpperInvariant()
                });
            }

            return result;
        }
    }
}
