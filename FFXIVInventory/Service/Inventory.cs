﻿namespace FFXIVInventory.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Inventory
    {
        public Inventory(string type, List<Cell> cells)
        {
            this.Type = type;
            this.Cells = new List<Cell>(cells);
        }

        public string Type { get; set; }

        public List<Cell> Cells { get; }
    }
}
