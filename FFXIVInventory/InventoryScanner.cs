﻿namespace FFXIVInventory.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Sharlayan;
    using Sharlayan.Core;
    using Sharlayan.Models;
    using Sharlayan.Models.ReadResults;

    public class InventoryScanner : IDisposable
    {
        private readonly string workingDir;

        private Task<bool> scanTask;

        public InventoryScanner(string workingDir)
        {
            this.workingDir = workingDir;
            Directory.SetCurrentDirectory(this.workingDir);
            this.Init();
        }

        public static bool IsAttached()
        {
            return MemoryHandler.Instance.IsAttached;
        }

        public static InventoryResult ReadInventory()
        {
            CheckAttachState();
            InventoryResult result = Reader.GetInventory();
            if (result.InventoryContainers.TrueForAll(inv => inv.Items.Count == 0))
            {
                return null;
            }

            return result;
        }

        public static CurrentPlayer ReadPlayerCharacter()
        {
            CheckAttachState();
            CurrentPlayerResult result = Reader.GetCurrentPlayer();
            if (string.IsNullOrEmpty(result.CurrentPlayer.Name))
            {
                return null;
            }

            return result.CurrentPlayer;
        }

        public static ActorResult ReadActors()
        {
            ActorResult result = Reader.GetActors();
            Log($"Read {result.CurrentNPCs.Count} NPCs");

            return result;
        }

        private static void CheckAttachState()
        {
            if (!MemoryHandler.Instance.IsAttached)
            {
                throw new System.Exception("scanner is not ready");
            }
        }

        private InventoryScanner Init()
        {
            MemoryHandler.Instance.SignaturesFoundEvent += (source, e) =>
            {
                Log($"signature scan result: {e.Signatures.Keys}, cost: {e.ProcessingTime}");
                Log($"can read inventories: {Reader.CanGetInventory()}");
                this.scanTask.RunSynchronously();
            };
            MemoryHandler.Instance.ExceptionEvent += (source, e) =>
            {
                Trace.TraceError($"exception ocurred: {e.Exception}, {e.Exception.StackTrace}");
            };
            return this;
        }

        public Task<bool> Scan()
        {
            if (MemoryHandler.Instance.IsAttached)
            {
                return this.scanTask;
            }

            Process[] processes = Process.GetProcessesByName("ffxiv_dx11");
            Trace.TraceInformation($"found {processes.Length} processes");
            if (processes.Length == 0)
            {
                return Task.FromResult(false);
            }

            // supported: English, Chinese, Japanese, French, German, Korean
            string gameLanguage = "Chinese";

            // whether to always hit API on start to get the latest sigs based on patchVersion, or use the local json cache (if the file doesn't exist, API will be hit)
            bool useLocalCache = true;

            // patchVersion of game, or latest
            string patchVersion = "4.2.1";
            Process process = processes[0];
            ProcessModel processModel = new ProcessModel
            {
                Process = process,
                IsWin64 = true
            };

            if (this.scanTask != null)
            {
                this.scanTask.Dispose();
            }

            this.scanTask = new Task<bool>(() => true);

            // Do async scan
            MemoryHandler.Instance.SetProcess(processModel, gameLanguage, patchVersion, useLocalCache, false);

            return this.scanTask;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool flag)
        {
            if (this.scanTask != null)
            {
                this.scanTask.Dispose();
            }
        }

        private static void Log(string message)
        {
            Trace.TraceInformation($"Thread-{Thread.CurrentThread.ManagedThreadId} - {message}");
        }
    }
}
