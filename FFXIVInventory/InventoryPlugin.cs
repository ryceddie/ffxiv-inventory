﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Machina;
using Machina.FFXIV;

namespace FFXIVInventory
{
    public class InventoryPlugin
    {
        private FFXIVNetworkMonitor monitor;

        private String path;

        private int serial;

        public InventoryPlugin(String windowName)
        {
            var pid = new ProcessTCPInfo().GetProcessIDByWindowName(windowName);
            this.monitor = new FFXIVNetworkMonitor();
            this.monitor.ProcessID = pid;
        }

        public InventoryPlugin(uint pid)
        {
            this.monitor = new FFXIVNetworkMonitor();
            this.monitor.ProcessID = pid;
        }

        public void InitPlugin()
        {
            this.path = "/works/log/ffxiv/messages";
            this.serial = 0;

            Directory.CreateDirectory(this.path);
            this.monitor.MessageReceived = MessageReceived;
            this.monitor.Start();
            Trace.TraceInformation("inventory plugin started");
        }

        public void DeInitPlugin()
        {
            this.monitor.Stop();
            Trace.TraceInformation("inventory plugin stopped");
        }

        private void MessageReceived(String connection, long epoch, byte[] message)
        {
            // Process Message
            Trace.TraceInformation("received at: " + epoch);
            Interlocked.Increment(ref this.serial);
            using (FileStream fs = File.Create(this.path + "/received-" + epoch + "-" + this.serial))
            {
                fs.Write(message, 0, message.Length);
            }
        }
    }
}
