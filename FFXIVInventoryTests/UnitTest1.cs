using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using FFXIVInventory;
using Sharlayan;
using Sharlayan.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace FFXIVInventoryTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PrintCurrentDir()
        {
            Directory.SetCurrentDirectory("/works/ms/resources");
            Trace.TraceInformation("current dir: " + Directory.GetCurrentDirectory());
        }

        [TestMethod]
        public void TestMethod1()
        {
            var plugin = new InventoryPlugin("���ջ���XIV");
            plugin.InitPlugin();
            System.Threading.Thread.Sleep(120 * 1000);
            plugin.DeInitPlugin();
        }

        [TestMethod]
        public void FuncTest()
        {
            Func<string> foo = () => "abc";
            foo.BeginInvoke(delegate { }, foo);
        }

        [TestMethod]
        public void ParseActionJson()
        {
            var file = "/works/ms/resources/actions.json";
            var json = "";
            using (var streamReader = new StreamReader(file))
            {
                json = streamReader.ReadToEnd();
            }
            var serializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Populate
            };
            JsonConvert.DeserializeObject<ConcurrentDictionary<string, ActionItem>>(json, serializerSettings);
        }

        [TestMethod]
        public void TryMemoryParser()
        {
            Directory.SetCurrentDirectory("/works/ms/resources");

            Process[] processes = Process.GetProcessesByName("ffxiv_dx11");
            Trace.TraceInformation("found " + processes.Length + " processes");
            if (processes.Length > 0)
            {
                // supported: English, Chinese, Japanese, French, German, Korean
                string gameLanguage = "Chinese";
                // whether to always hit API on start to get the latest sigs based on patchVersion, or use the local json cache (if the file doesn't exist, API will be hit)
                bool useLocalCache = true;
                // patchVersion of game, or latest
                string patchVersion = "latest";
                Process process = processes[0];
                ProcessModel processModel = new ProcessModel
                {
                    Process = process, IsWin64 = true
                };

                MemoryHandler.Instance.SetProcess(processModel, gameLanguage, patchVersion, useLocalCache);

                var inventories = Reader.GetInventoryItems();
                Trace.TraceInformation("read " + inventories.InventoryEntities.Count + " inventories");
            }
        }

        public void AnalyzeMessages()
        {
            var filenames = Directory.EnumerateFiles("/works/log/ffxiv/messages");
            foreach (var filename in filenames)
            {
                using (var fs = File.OpenRead(filename))
                {
                    var buffer = new byte[32];
                    fs.Read(buffer, 0, 32);
                }
            }
        }
    }
}
